#!/bin/sh 

shout() { echo "$0: $*" >&2; }
die() { shout "$*"; exit 111; }
try() { "$@" || die "ERROR: cannot $*"; }
