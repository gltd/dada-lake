SELECT
    'CREATE DATABASE dada'
WHERE
    NOT EXISTS (
        SELECT
        FROM
            pg_database
        WHERE
            datname = 'dada'
    );
