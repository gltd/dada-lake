#!/bin/sh

set -x

echo "Waiting for redis..."
while ! nc -z redis 6379; do
  sleep 1
done
while ! nc -z postgres 5432; do
  sleep 1
done
echo "Redis started..."
echo "Starting listener..."
make run-listener env=docker
