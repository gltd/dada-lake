#!/usr/bin/env python
from typing import Generator, Tuple

from dada_utils import path
from dada_log import DadaLogger

ROOT_DIR = path.here(__file__, "..")
LIB_DIR = path.join(ROOT_DIR, "lib/")
LIB_REQUIRES = ["dada-settings", "dada-utils", "dada-log", "dada-plugin", "dada-errors", "dada-types", "dada-text"]
CURRENT_VERSION = open(path.join(ROOT_DIR, ".dada-version")).read().strip()
log = DadaLogger("lib-install-dev")


def gen_commands() -> Generator[None, None, Tuple[str, str]]:
    """ """
    for fp in sorted(path.list_files(LIB_DIR)):
        if not fp.endswith("setup.py"):
            continue

        lib_name = fp.split("/")[-2]
        if lib_name == "dada-lake":
            continue
        cmd = f"pip install -e '{path.join(LIB_DIR, lib_name)}'"
        yield cmd, lib_name


def run_command(args: str) -> None:
    cmd, lib_name = args
    p = path.exec(cmd)
    if p.ok:
        log.info(f"Successfully installed: {lib_name}")
    else:
        msg = f"Could not install {lib_name} because: {p.stderr}"
        log.error(msg)
        raise RuntimeError(msg)


def main() -> None:
    """
    Install all dada libs in editable model locally.
    """
    # pool = Pool(10)
    # pool.map(run_command, gen_commands())
    for cmd in gen_commands():
        run_command(cmd)


if __name__ == "__main__":
    main()
