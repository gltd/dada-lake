import importlib
import logging
import os

import dada_settings
import flask
from celery import Celery
from dada_serde import json_to_obj, obj_to_json
from dada_types.base import SerializableObject

from dada.models.core import rdsconn

# after-trigger stream

log = logging.getLogger()


class EventStream(SerializableObject):
    def __init__(self, rdsconn=rdsconn):
        self.rdsconn = rdsconn

    def _parse_request(self, request) -> dict:
        """Parse a flask request object into a dict"""
        return {
            "collection": request.blueprint,
            "endpoint": request.endpoint,
            "data": request.get_data().decode("utf-8"),
            "args": request.args,
        }

    def _parse_response(self, response) -> dict:
        """Parse a flask response object into a dict"""
        return json_to_obj(
            list(response.__dict__.get("response", ["{}"]))[0].decode("utf-8")
        )

    def serialize_request(
        self, request: flask.Request, response: flask.Response
    ):
        """Serialize a flask request and response into a json string"""
        return obj_to_json(
            {
                "request": self._parse_request(request),
                "response": self._parse_response(response),
                "producer": dada_settings.REDIS_STREAM_PRODUCER_NAME,
            }
        )

    def write(self, request: flask.Request, response: flask.Response):
        """Emit a request to the redis stream"""
        data = self.serialize_request(request, response)
        log.debug(
            f"SENDING data to stream {dada_settings.REDIS_STREAM_NAME}: {data}"
        )
        try:
            return self.rdsconn.xadd(
                dada_settings.REDIS_STREAM_NAME,
                fields={"data": data},
            )

        except ConnectionError as e:
            raise ("ERROR CREATING REDIS CONNECTION: {}".format(e))


event_stream = EventStream()


def get_celery_tasks_from_module(module_name):
    """
    Discover a list of task modules to pass to `include`
    """
    modules = []
    mod = importlib.import_module(module_name)
    package_name = mod.__name__
    package_path = mod.__path__[0]
    for fp in os.listdir(package_path):
        if (
            not fp.endswith(".pyc")
            and "__init__" not in fp
            and "templates" not in fp
        ):
            name = fp.replace(".py", "")
            modules.append(f"{package_name}.{name}")
    return modules


def make_celery(app_name):
    """
    Make a celery app
    """
    return Celery(
        app_name,
        backend=dada_settings.CELERY_RESULT_BACKEND,
        broker=dada_settings.CELERY_BROKER_URL,
        include=get_celery_tasks_from_module("dada.queue"),
    )


def init_celery(celery, app):
    """
    Init the celery task (for use in factory)
    """
    celery.conf.update(app.config)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask


# make celery

celery = make_celery(__name__)
