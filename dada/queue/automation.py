import logging
from typing import Any

from dada.models.automation import Automation
from dada.queue.core import celery

log = logging.getLogger()


@celery.task
def run(automation_id: int, input: Any, **kwargs):
    """
    Upload a file to s3 as a background task
    """
    automation = Automation.get_by_id(automation_id)
    log.info(f"[Q.automation.run] running {automation.name}")
    result = automation.run(input, **kwargs)
    log.info(f"[Q.automation.run] {automation.name} complete")