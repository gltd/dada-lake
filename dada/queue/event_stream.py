import json
import logging
import time
from dada_types.base import SerializableObject
import flask

from dada.models.automation import Automation
from dada.models.core import rdsconn
from dada_serde import json_to_obj, obj_to_json
import dada_settings

from dada.queue import automation

# after-trigger stream

log = logging.getLogger()


class EventStream(SerializableObject):
    def __init__(self, rdsconn=rdsconn):
        self.rdsconn = rdsconn

    def _parse_request(self, request) -> dict:
        """Parse a flask request object into a dict"""
        return {
            "collection": request.blueprint,
            "endpoint": request.endpoint,
            "data": request.get_data().decode("utf-8"),
            "args": request.args,
        }

    def _parse_response(self, response) -> dict:
        """Parse a flask response object into a dict"""
        return json_to_obj(
            list(response.__dict__.get("response", ["{}"]))[0].decode("utf-8")
        )

    def serialize_request(self, request: flask.Request, response: flask.Response):
        """Serialize a flask request and response into a json string"""
        return obj_to_json(
            {
                "request": self._parse_request(request),
                "response": self._parse_response(response),
                "producer": dada_settings.REDIS_STREAM_PRODUCER_NAME,
            }
        )

    def write(self, request: flask.Request, response: flask.Response):
        """Emit a request to the redis stream"""
        data = self.serialize_request(request, response)
        log.debug(f"SENDING data to stream {dada_settings.REDIS_STREAM_NAME}: {data}")
        try:
            return self.rdsconn.xadd(
                dada_settings.REDIS_STREAM_NAME,
                fields={"data": data},
            )

        except ConnectionError as e:
            raise ("ERROR CREATING REDIS CONNECTION: {}".format(e))

    def read_one(self, last_id=0, sleep_ms=5000):
        """Read one message from the redis stream"""
        resp = self.rdsconn.xread(
            {dada_settings.REDIS_STREAM_NAME: last_id}, count=1, block=sleep_ms
        )
        if resp:
            key, messages = resp[0]
            last_id, data = messages[0]
            return last_id, list(data.values())[0]
        else:
            return None, None

    def deserialize(self, data):
        """Deserialize a json string into a dict"""
        return json_to_obj(data)

    def read(self, last_id=0, sleep_ms=5000):
        """Read from the redis stream"""
        while True:
            last_id, data = self.read_one(last_id, sleep_ms)
            if not data:
                time.sleep(sleep_ms / 1000)
                continue
            yield self.deserialize(data)

    def run_automations(self, last_id=0, sleep_ms=5000, **kwargs):
        """Trigger automations from the redis stream"""
        for message in self.read(last_id, sleep_ms):
            for automation_id, input in Automation.find_automations_for_stream_message(
                message
            ):
                log.info("Triggering automation: {}".format(automation_id))
                automation.run(automation_id, input, **kwargs)


event_stream = EventStream()


def listener():
    """Handle stream automations 'after' the request has been handled. This function exists so that it can be called via the CLI"""
    event_stream.run_automations()


if __name__ == "__main__":
    listener()
