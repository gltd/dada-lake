from flask import Blueprint

from dada.models.plugin import Plugin
from dada.models.tag_join_table import PluginTag
from dada.models.api_response import PluginResponse
from dada.resources.core import crud_blueprint_from_model

# ///////////////
# Plugins API
# ///////////////
SEARCH_DOCS = """
"""
UPSERT_DOCS = """
"""
FETCH_DOCS = """
"""
DELETE_DOCS = """
"""

PLUGINS_APP = crud_blueprint_from_model(
    blueprint=Blueprint("plugins", __name__),
    core_model=Plugin,
    base_response_schema=PluginResponse,
    doc_tags=["plugins"],
    relationships=[PluginTag],
    # search_docs=SEARCH_DOCS,
    # upsert_docs=UPSERT_DOCS,
    # fetch_docs=FETCH_DOCS,
    # delete_docs=DELETE_DOCS
)
