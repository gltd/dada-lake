from flask import Blueprint

from dada.models.automation import Automation
from dada.models.tag_join_table import AutomationTag
from dada.models.api_response import AutomationResponse
from dada.resources.core import crud_blueprint_from_model

# ///////////////
# Automations API
# ///////////////
SEARCH_DOCS = """
"""
UPSERT_DOCS = """
"""
FETCH_DOCS = """
"""
DELETE_DOCS = """
"""

AUTOMATIONS_APP = crud_blueprint_from_model(
    blueprint=Blueprint("automations", __name__),
    core_model=Automation,
    base_response_schema=AutomationResponse,
    doc_tags=["automations"],
    relationships=[AutomationTag],
    # search_docs=SEARCH_DOCS,
    # upsert_docs=UPSERT_DOCS,
    # fetch_docs=FETCH_DOCS,
    # delete_docs=DELETE_DOCS
)
