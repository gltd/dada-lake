from flask import Blueprint

from dada.models.function import Function
from dada.models.function_automation import FunctionAutomation
from dada.models.tag_join_table import FunctionTag
from dada.models.api_response import FunctionResponse
from dada.resources.core import crud_blueprint_from_model

# ///////////////
# Functions API
# ///////////////
SEARCH_DOCS = """
"""
UPSERT_DOCS = """
"""
FETCH_DOCS = """
"""
DELETE_DOCS = """
"""

FUNCTIONS_APP = crud_blueprint_from_model(
    blueprint=Blueprint("functions", __name__),
    core_model=Function,
    base_response_schema=FunctionResponse,
    doc_tags=["functions"],
    relationships=[FunctionTag, FunctionAutomation],
    # search_docs=SEARCH_DOCS,
    # upsert_docs=UPSERT_DOCS,
    # fetch_docs=FETCH_DOCS,
    # delete_docs=DELETE_DOCS
)
