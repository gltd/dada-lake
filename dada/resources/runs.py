from flask import Blueprint

from dada.models.run import Run
from dada.models.tag_join_table import RunTag
from dada.models.api_response import RunResponse
from dada.resources.core import crud_blueprint_from_model

# ///////////////
# Runs API
# ///////////////
SEARCH_DOCS = """
"""
UPSERT_DOCS = """
"""
FETCH_DOCS = """
"""
DELETE_DOCS = """
"""

RUNS_APP = crud_blueprint_from_model(
    blueprint=Blueprint("runs", __name__),
    core_model=Run,
    base_response_schema=RunResponse,
    doc_tags=["runs"],
    relationships=[RunTag],
    # search_docs=SEARCH_DOCS,
    # upsert_docs=UPSERT_DOCS,
    # fetch_docs=FETCH_DOCS,
    # delete_docs=DELETE_DOCS
)
