"""
"""

import logging

import dada_settings
from dada_types import T

from dada.models.base import DBTable
from dada.models.core import db
from dada.models.mixin import GroupTableMixin
from dada.models.field import FieldCacheMixin
from dada.models.tag_join_table import RunTag
from dada.models.theme_table import RunTheme


RUN_MODEL_LOGGER = logging.getLogger(__name__)


class Run(DBTable, GroupTableMixin, FieldCacheMixin):

    __tablename__ = "run"
    __module__ = "dada.models.run"
    __defaults__ = dada_settings.FOLDER_DEFAULTS
    __id_fields__ = ["id", "name", "slug"]
    __tag_join_table__ = RunTag
    __theme_table__ = RunTheme

    # every function comes from an automation
    automation_id = db.Column(
        T.fk.col, db.ForeignKey("automation.id"), index=True
    )

    automation = db.relationship("Automation", lazy="joined")

    # indices

    __table_args__ = (
        db.Index("run_vector_idx", "vector", postgresql_using="gin"),
    )
