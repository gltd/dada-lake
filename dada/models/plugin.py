"""
A Plugin is a collection of dada Functions that can be installed via pip
"""


import logging

from dada_types import T

from dada.models.base import DBTable
from dada.models.core import db
from dada.models.mixin import GroupTableMixin
from dada.models.field import FieldCacheMixin
from dada.models.tag_join_table import PluginTag
from dada.models.theme_table import PluginTheme


log = logging.getLogger(__name__)


class Plugin(DBTable, GroupTableMixin, FieldCacheMixin):

    __tablename__ = "plugin"
    __module__ = "dada.models.plugin"
    __id_fields__ = ["id", "name", "slug"]
    __snake_columns__ = ["name"]
    __tag_join_table__ = PluginTag
    __theme_table__ = PluginTheme

    # relationships

    functions = db.relationship("Function", lazy="joined")
    tags = db.relationship("Tag", secondary="plugin_tag", lazy="joined")

    # indices

    __table_args__ = (
        db.Index(f"plugin_name_uniq_idx", "name", unique=True),
        db.Index(f"plugin_slug_uniq_idx", "slug", unique=True),
        db.Index("plugin_vector_idx", "vector", postgresql_using="gin"),
    )
