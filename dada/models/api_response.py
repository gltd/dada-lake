"""
Marshmallow Response Objects Via Flask-Marshmallow

See: https://marshmallow.readthedocs.io/en/stable/api_reference.html

TODO: Can we just do this inside of resources.core now?

"""
from marshmallow import fields

from dada_types import T, NewVal

from dada.models.core import db, ma
from dada.models.user import User
from dada.models.tag import Tag
from dada.models.file import File
from dada.models.field import Field, FieldTheme
from dada.models.folder import Folder
from dada.models.desktop import Desktop
from dada.models.file_folder import FileFolder
from dada.models.folder_desktop import FolderDesktop
from dada.models.function import Function
from dada.models.automation import Automation
from dada.models.function_automation import FunctionAutomation
from dada.models.run import Run
from dada.models.plugin import Plugin
from dada.models.tag_join_table import *  # noqa: F401, F403
from dada.models.theme_table import *  # noqa: F401, F403

# ///////////////////
# Base Response
# ///////////////////
class BaseResponse(ma.SQLAlchemyAutoSchema):
    class Meta:
        sqla_session = db.session
        strict = True


## /////////////////////////////////
### File
## /////////////////////////////////


class FileFolderResponse(BaseResponse):
    __dada_type__ = "file_folder"

    class Meta:
        model = FileFolder


class FolderDesktopResponse(BaseResponse):
    __dada_type__ = "folder_desktop"

    class Meta:
        model = FolderDesktop


# ///////////////////////
# Theme Table Responses
# //////////////////////


class FileThemeResponse(BaseResponse):
    __dada_type__ = "file_theme"

    class Meta:
        model = FileTheme


class FolderThemeResponse(BaseResponse):
    __dada_type__ = "folder_theme"

    class Meta:
        model = FolderTheme


class DesktopThemeResponse(BaseResponse):
    __dada_type__ = "desktop_theme"

    class Meta:
        model = DesktopTheme


class TagThemeResponse(BaseResponse):
    __dada_type__ = "tag_theme"

    class Meta:
        model = TagTheme


class FieldThemeResponse(BaseResponse):
    __dada_type__ = "field_theme"

    class Meta:
        model = FieldTheme


class UserThemeResponse(BaseResponse):
    __dada_type__ = "user_theme"

    class Meta:
        model = TagTheme


class AutomationThemeResponse(BaseResponse):
    __dada_type__ = "automation_theme"

    class Meta:
        model = AutomationTheme


class FunctionThemeResponse(BaseResponse):
    __dada_type__ = "function_theme"

    class Meta:
        model = FunctionTheme


class RunThemeResponse(BaseResponse):
    __dada_type__ = "run_theme"

    class Meta:
        model = RunTheme


# ////////////////////////
# Tag Join Table Responses
# ///////////////////////


class FileTagResponse(BaseResponse):
    __dada_type__ = "file_tag"

    class Meta:
        model = FileTag


class FolderTagResponse(BaseResponse):
    __dada_type__ = "folder_tag"

    class Meta:
        model = FolderTag


class DesktopTagResponse(BaseResponse):
    __dada_type__ = "desktop_tag"

    class Meta:
        model = DesktopTag


class TagTagResponse(BaseResponse):
    __dada_type__ = "tag_tag"

    class Meta:
        model = TagTag


class FieldTagResponse(BaseResponse):
    __dada_type__ = "field_tag"

    class Meta:
        model = FieldTag


class UserTagResponse(BaseResponse):
    __dada_type__ = "user_tag"

    class Meta:
        model = UserTag


# ///////////////////
# low-level models
# ////////////////////


class TagResponse(BaseResponse):
    __dada_type__ = "tag"

    theme = NewVal("tag_theme", fields.Nested(TagThemeResponse))

    class Meta:
        model = Tag
        exclude = ["vector"]


class FieldResponse(BaseResponse):
    __dada_type__ = "field"

    core = T.bool.val

    tags = NewVal("field_tag_array", fields.List(fields.Nested(TagResponse)))
    theme = NewVal("field_theme", fields.Nested(FieldThemeResponse))

    class Meta:
        model = Field
        exclude = ["vector"]


# ////////////////////////////
# Core Models
# ////////////////////////////


class DesktopResponse(BaseResponse):
    __dada_type__ = "desktop"

    # relationships
    folders = NewVal(
        "desktop_folder_array",
        fields.List(fields.Nested(FolderDesktopResponse)),
    )
    tags = NewVal("desktop_tag_array", fields.List(fields.Nested(TagResponse)))
    theme = NewVal("desktop_theme", fields.Nested(DesktopThemeResponse))

    class Meta(BaseResponse.Meta):
        model = Desktop
        exclude = ["vector"]


class DesktopsForFolderResponse(DesktopResponse):
    pass


class FolderResponse(BaseResponse):
    __dada_type__ = "folder"

    # relationships
    files = NewVal(
        "folder_file_array", fields.List(fields.Nested(FileFolderResponse))
    )
    desktops = NewVal(
        "folder_desktop_array",
        fields.List(fields.Nested(DesktopsForFolderResponse)),
    )
    tags = NewVal("folder_tag_array", fields.List(fields.Nested(TagResponse)))
    theme = NewVal("folder_theme", fields.Nested(FolderThemeResponse))

    class Meta(BaseResponse.Meta):
        exclude = ["vector"]
        model = Folder


class FoldersForFileResponse(FolderResponse):
    pass


class FileResponse(BaseResponse):
    __dada_type__ = "file"
    theme = NewVal("file_theme", fields.Nested(FileThemeResponse))

    # urls
    s3_urls = fields.Dict()

    human_size = T.text.val

    folders = NewVal(
        "folder_array", fields.List(fields.Nested(FoldersForFileResponse))
    )
    tags = NewVal("folder_tag_array", fields.List(fields.Nested(TagResponse)))

    class Meta(BaseResponse.Meta):
        model = File
        exclude = ["vector"]


class UserResponse(BaseResponse):

    __dada_type__ = "user"

    tags = NewVal("user_tag_array", fields.List(fields.Nested(TagResponse)))
    files = NewVal(
        "user_file_array",
        fields.List(
            fields.Nested(
                FileResponse,
                only=("id", "slug"),
            )
        ),
    )
    folders = NewVal(
        "user_folder_array",
        fields.List(fields.Nested(FolderResponse, only=("id", "slug"))),
    )
    desktops = NewVal(
        "user_desktop_array",
        fields.List(fields.Nested(DesktopResponse, only=("id", "slug"))),
    )
    theme = NewVal("user_theme", fields.Nested(UserThemeResponse))

    class Meta(BaseResponse.Meta):
        model = User
        exclude = ["vector"]


class FunctionAutomationResponse(BaseResponse):
    __dada_type__ = "function_automation"

    class Meta:
        model = FunctionAutomation


class FunctionResponse(BaseResponse):
    __dada_type__ = "function"

    tags = NewVal(
        "function_tag_array", fields.List(fields.Nested(TagResponse))
    )
    theme = NewVal("function_theme", fields.Nested(FunctionThemeResponse))

    class Meta(BaseResponse.Meta):
        model = Function
        exclude = ["vector"]


class AutomationResponse(BaseResponse):
    __dada_type__ = "automation"

    functions = NewVal(
        "automation_function_array",
        fields.List(fields.Nested(FunctionResponse)),
    )
    tags = NewVal(
        "automation_tag_array", fields.List(fields.Nested(TagResponse))
    )
    theme = NewVal("automation_theme", fields.Nested(AutomationThemeResponse))

    class Meta(BaseResponse.Meta):
        model = Automation
        exclude = ["vector"]


class RunResponse(BaseResponse):
    __dada_type__ = "run"

    tags = NewVal("run_tag_array", fields.List(fields.Nested(TagResponse)))
    theme = NewVal("run_theme", fields.Nested(RunThemeResponse))
    automation = NewVal("run_automation", fields.Nested(AutomationResponse))

    class Meta(BaseResponse.Meta):
        model = Run
        exclude = ["vector"]


class PluginResponse(BaseResponse):
    __dada_type__ = "plugin"

    functions = NewVal(
        "plugin_function_array", fields.List(fields.Nested(FunctionResponse))
    )

    class Meta(BaseResponse.Meta):
        model = Plugin
        exclude = ["vector"]
