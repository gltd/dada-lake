import logging
from typing import Any, Dict, Generator, List, Optional, Tuple

from dada.models.base import DBTable
from dada.models.core import db
from dada.models.mixin import GroupTableMixin, UserMixin
from dada.models.field import FieldCacheMixin
from dada.models.tag_join_table import AutomationTag
from dada.models.theme_table import AutomationTheme

from dada_settings import AUTOMATION_TRIGGER_TYPES
from dada_types import T
from dada_types.flt import PyFilterStrings


log = logging.getLogger()


class Automation(DBTable, GroupTableMixin, UserMixin, FieldCacheMixin):
    __tablename__ = "automation"
    __module__ = "dada.models.automation"
    __id_fields__ = ["id", "user_id", "name", "slug"]
    __tag_join_table__ = AutomationTag
    __theme_table__ = AutomationTheme


    # fields
    trigger_type = T.enum.col(
        *AUTOMATION_TRIGGER_TYPES,
        name="automation_trigger_types_enum",
        create_type=True,
        index=True
    )
    trigger_endpoints = db.Column(T.text_array.col, index=True) # a list of endpoints to trigger this automation on. only applies for stream triggers
    trigger_filters = db.Column(T.filter_array.col, default=[]) # filters to apply against api responses for the trigger. only applies for stream triggers
    trigger_filter_combinator = T.enum.col(
        "and",
        "or",
        name="automation_filter_combinator_enum",
        create_type=True,
    )
    trigger_cron = db.Column(T.cron.col, nullable=True)            # cron expression for the trigger. only applies for cron automations


    # relationships
    functions = db.relationship(
        "Function", secondary="function_automation", lazy="joined"
    )
    tags = db.relationship("Tag", secondary="automation_tag", lazy="joined")
    theme = db.relationship("AutomationTheme", lazy="joined")

    # indices

    __table_args__ = (
        db.Index("automation_name_uniq_idx", "name", "user_id", unique=True),
        db.Index("automation_slug_uniq_idx", "slug", "user_id", unique=True),
        db.Index("automation_vector_idx", "vector", postgresql_using="gin"),
    )

    @property
    def filters(self):
        PyFilterStrings(self.trigger_filters, combinator=self.trigger_filter_combinator)

    @classmethod
    def find_automations_for_stream_message(cls, message) -> Generator[None, None, Tuple[int, List[Dict[str, Any]]]]:
        """find all automations that match the stream message and yield a tuple of the automation id and the rows that matched the filters"""
        print(message)
        endpoint = message["request"].get("endpoint")
        automations = db.session.query(cls).filter(cls.trigger_endpoints.any(endpoint)).all()
        response = message.get("response", {})
        if not isinstance(response, list):
            response = [response]

        # for each row of the response and each automation, check if the automation's filters match the row
        for automation in automations:
            input = []
            for row in response:
                if automation.filters.match(row):
                    input.append(row)
            yield automation.id, input

