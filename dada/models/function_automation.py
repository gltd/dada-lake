from sqlalchemy import ForeignKey, Index

from dada.models.base import DBTable
from dada.models.core import db
from dada.models.field import FieldCacheMixin
from dada.models.mixin import JoinFieldTableMixin, PositionalTableMixin


class FunctionAutomation(
    DBTable, JoinFieldTableMixin, PositionalTableMixin, FieldCacheMixin
):

    __tablename__ = "function_automation"
    __module__ = "dada.models.function_automation"
    __from_id__ = "function_id"
    __to_id__ = "automation_id"

    function_id = db.Column(db.Integer, ForeignKey("function.id"), index=True)
    automation_id = db.Column(
        db.Integer, ForeignKey("automation.id"), index=True
    )

    # relationships

    function = db.relationship("Function", lazy=True)
    automation = db.relationship("Automation", lazy=True)

    # indices
    __table_args__ = (
        Index(
            "function_automation_uniq_idx",
            "function_id",
            "automation_id",
            "position",
            unique=True,
        ),
    )
