"""
Tag Join Tables
TODO: figure out how to dry this up?
"""
import logging

from dada_types import T

from dada.models.base import DBTable
from dada.models.core import db
from dada.models.mixin import (
    TagTableMixin,
    JoinTableMixin,
    JoinTableMixin,
)


TAG_JOIN_MODEL_LOGGER = logging.getLogger()


class FileTag(DBTable, TagTableMixin, JoinTableMixin):

    __tablename__ = "file_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "file_id"

    file_id = db.Column(T.file_id.col, db.ForeignKey("file.id"), index=True)
    file = db.relationship("File", lazy=True)


class FieldTag(DBTable, TagTableMixin, JoinTableMixin):

    __tablename__ = "field_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "field_id"

    field_id = db.Column(T.field_id.col, db.ForeignKey("field.id"), index=True)
    field = db.relationship("Field", lazy=True)


class FolderTag(DBTable, TagTableMixin, JoinTableMixin):

    __tablename__ = "folder_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "folder_id"

    folder_id = db.Column(
        T.folder_id.col, db.ForeignKey("folder.id"), index=True
    )
    folder = db.relationship("Folder", lazy=True)


class DesktopTag(DBTable, TagTableMixin, JoinTableMixin):

    __tablename__ = "desktop_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "desktop_id"

    desktop_id = db.Column(
        T.desktop_id.col, db.ForeignKey("desktop.id"), index=True
    )
    desktop = db.relationship("Desktop", lazy=True)


class UserTag(DBTable, TagTableMixin, JoinTableMixin):

    __tablename__ = "user_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "user_id"

    user_id = db.Column(T.user_id.col, db.ForeignKey("user.id"), index=True)
    user = db.relationship("User", lazy=True)


# //////////////////////////////////////////////////////////////////
# Functions/Automations/Plugins/Runs
# //////////////////////////////////////////////////////////////////


class FunctionTag(DBTable, TagTableMixin, JoinTableMixin):

    __tablename__ = "function_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "function_id"

    function_id = db.Column(T.pk.col, db.ForeignKey("function.id"), index=True)
    function = db.relationship("Function", lazy=True)


class AutomationTag(DBTable, TagTableMixin, JoinTableMixin):

    __tablename__ = "automation_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "automation_id"

    automation_id = db.Column(
        T.pk.col, db.ForeignKey("automation.id"), index=True
    )
    automation = db.relationship("Automation", lazy=True)


class PluginTag(DBTable, TagTableMixin, JoinTableMixin):

    __tablename__ = "plugin_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "plugin_id"

    plugin_id = db.Column(T.pk.col, db.ForeignKey("plugin.id"), index=True)
    plugin = db.relationship("Plugin", lazy=True)


class RunTag(DBTable, TagTableMixin, JoinTableMixin):

    __tablename__ = "run_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "run_id"

    run_id = db.Column(T.pk.col, db.ForeignKey("run.id"), index=True)
    run = db.relationship("Run", lazy=True)


# //////////////////////////////////////////////////////////////////
# Etc.
# //////////////////////////////////////////////////////////////////


class TagTag(DBTable, JoinTableMixin):
    """
    A mapping of tagged tags?
    """

    __tablename__ = "tag_tag"
    __module__ = "dada.models.tag_join_table"
    __from_id__ = "from_tag_id"
    __to_id__ = "to_tag_id"

    from_tag_id = db.Column(T.tag_id.col, db.ForeignKey("tag.id"), index=True)
    to_tag_id = db.Column(T.tag_id.col, db.ForeignKey("tag.id"), index=True)
    from_tag = db.relationship("Tag", foreign_keys=[from_tag_id], lazy=True)
    to_tag = db.relationship("Tag", foreign_keys=[to_tag_id], lazy=True)
