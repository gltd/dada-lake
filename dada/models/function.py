"""
A Function takes a set of parameters and/or a stream of dada Entities and
performs transformations via custom python code and the dada-lake
"""

import logging

import dada_settings
from dada_types import T, Parameters

from dada.models.base import DBTable
from dada.models.core import db
from dada.models.mixin import GroupTableMixin
from dada.models.field import FieldCacheMixin
from dada.models.tag_join_table import FunctionTag
from dada.models.theme_table import FunctionTheme


FUNCTION_MODEL_LOGGER = logging.getLogger(__name__)


class Function(DBTable, GroupTableMixin, FieldCacheMixin):

    __tablename__ = "function"
    __module__ = "dada.models.function"
    __id_fields__ = ["id", "plugin_id", "name", "slug"]
    __snake_columns__ = ["name"]
    __tag_join_table__ = FunctionTag
    __theme_table__ = FunctionTheme
    _parameters = None

    # every function comes from a plugin
    plugin_id = db.Column(T.fk.col, db.ForeignKey("plugin.id"), index=True)

    # parameter definition
    param_def = db.Column(T.param_def.col, default="{}")

    # what type(s) of files does this function accept as input
    input_file_types = db.Column(
        T.file_type_array.col(
            T.file_type.col(
                *(dada_settings.FILE_DEFAULTS_FILE_TYPES + ["all"]),
                name=f"function_input_file_types_enum",
                create_type=True,
            )
        ),
        index=True,
        default=["all"],
    )
    # what type(s) of file subtypes does this function accept as input
    input_file_subtypes = db.Column(
        T.file_subtype_array.col(
            T.file_subtype.col(
                *(dada_settings.FILE_DEFAULTS_FILE_SUBTYPES + ["all"]),
                name=f"function_input_file_subtypes_enum",
                create_type=True,
            )
        ),
        index=True,
        default=["all"],
    )

    # the list of entities a function accepts (file, tag, etc.)
    input_entity_types = db.Column(
        T.entity_type_array.col(
            T.entity_type.col(
                *dada_settings.FUNCTION_DEFAULTS_ENTITY_TYPES,
                name="function_input_entity_types_enum",
                create_type=True,
            )
        ),
        index=True,
        default=[],
    )

    # functions limited by entity type output
    output_file_types = db.Column(
        T.file_type_array.col(
            T.file_type.col(
                *(dada_settings.FILE_DEFAULTS_FILE_TYPES + ["all"]),
                name=f"function_output_file_types_enum",
                create_type=True,
            )
        ),
        index=True,
        default=["all"],
    )
    output_file_subtypes = db.Column(
        T.file_subtype_array.col(
            T.file_subtype.col(
                *(dada_settings.FILE_DEFAULTS_FILE_SUBTYPES + ["all"]),
                name=f"function_output_file_subtypes_enum",
                create_type=True,
            )
        ),
        index=True,
        default=["all"],
    )
    output_entity_types = db.Column(
        T.entity_type_array.col(
            T.entity_type.col(
                *dada_settings.FUNCTION_DEFAULTS_ENTITY_TYPES,
                name="function_output_entity_types_enum",
                create_type=True,
            )
        ),
        index=True,
        default=[],
    )

    # relationships
    plugin = db.relationship("Plugin", lazy="joined")
    automations = db.relationship(
        "Automation", secondary="function_automation", lazy="joined"
    )
    tags = db.relationship("Tag", secondary="function_tag", lazy="joined")

    # indices

    __table_args__ = (
        db.Index(f"function_name_uniq_idx", "name", unique=True),
        db.Index(f"function_slug_uniq_idx", "slug", unique=True),
        db.Index("function_vector_idx", "vector", postgresql_using="gin"),
    )

    @property
    def parameters(self):
        if not self._parameters:
            self._parameters = Parameters(**self.param_def)
        return self._parameters
