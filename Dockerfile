FROM python:3.10.0-buster
MAINTAINER gltd <hey@gltd.email>

# set env
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive
	
# Install system requirements
RUN apt update
RUN apt install -y \
	curl \
	build-essential \
	libldap2-dev \
	libsasl2-dev \
	libtag1-dev \
	python3-pip \
	ffmpeg \
	libpq-dev \
	uwsgi \
	uwsgi-plugin-python3 \
	netcat \
	git \
	&& rm -rf /var/lib/apt/lists/*
RUN pip3.10 install --upgrade pip
# https://stackoverflow.com/questions/49911550/how-to-upgrade-disutils-package-pyyaml
RUN pip3.10 install --ignore-installed PyYAML
# Configure system

RUN export PATH=~/.local/bin:$PATH
ENV PYTHONPATH "/opt/dada:$PYTHONPATH"

# Install API requirements

WORKDIR /opt/dada

COPY requirements.txt .
RUN pip3.10 install -r requirements.txt

# Install API

COPY . .
RUN make install

# Setup public temp directory
RUN mkdir -p /tmp
RUN chown -R 777 /tmp

# Start API Server
CMD ["/opt/dada/scripts/api-entrypoint.sh"]
