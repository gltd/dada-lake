TAG_DEFAULTS_ACCEPTS_ENTITY_TYPES = [
    "file",
    "field",
    "folder",
    "desktop",
    "user",
    "theme",
]

TAG_DEFAULTS_ACCEPTS_ENTITY_TYPES_ALL = TAG_DEFAULTS_ACCEPTS_ENTITY_TYPES + [
    "all"
]
TAG_DEFAULTS_ACCEPTS_ENTITY_TYPES_DEFAULT = ["all"]
