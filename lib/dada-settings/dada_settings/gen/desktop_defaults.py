# ///////////////////////////////////////
# Random Desktop Generation
# ///////////////////////////////////////


DESKTOP_DEFAULTS_RANDOM_DESKTOP_NAME_WORD_1 = [
    "dev",
    "prod",
    "test",
    "bar",
    "baz",
    "uqbar",
    "jazz",
    "funk",
    "rhythm",
    "ambient",
]
DESKTOP_DEFAULTS_RANDOM_DESKTOP_NAME_WORD_2 = [
    "space",
    "zone",
    "collective",
    "habitat",
    "forest",
    "meadow",
    "function",
    "anti-platform",
    "environment",
]
