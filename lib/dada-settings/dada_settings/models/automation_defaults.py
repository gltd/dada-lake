AUTOMATION_TRIGGER_TYPES = [
    "cron", # This automation runs on a regular schedule
    "manual", # This automation only runs when manually triggered
    "stream",  # This automation runs in response to requests made against the API
]
