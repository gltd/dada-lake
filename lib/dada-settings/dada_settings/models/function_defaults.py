# ///////////////////////////////////////
# FUNCTION ACCEPTS ENTITY TYPES
# ///////////////////////////////////////

FUNCTION_DEFAULTS_ENTITY_TYPES = [
    "file",
    "folder",
    "field",
    "file_folder",
    "folder_desktop",
    "desktop",
    "tag",
    "user",
]
