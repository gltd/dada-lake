from dada_settings.utils import join_fields

# /////////////////
# DEFAULT AUDIO FIELDS
# ////////////
AUDIO_DEFAULTS_DEFAULT_FIELD_PROPS = {
    "accepts_entity_types": ["file"],
    "accepts_file_types": ["audio"],
}

AUDIO_DEFAULTS_DEFAULT_FIELDS_INIT = [
]

AUDIO_DEFAULTS_DEFAULT_FIELDS = join_fields(
    "audio",
    AUDIO_DEFAULTS_DEFAULT_FIELDS_INIT,
    AUDIO_DEFAULTS_DEFAULT_FIELD_PROPS,
)