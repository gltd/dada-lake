import argparse
import logging
import sys
from typing import Any, Dict, List, Optional, Tuple
from uuid import uuid4

import redis
from dada_client import DadaClient
from dada_file import DadaFile
from dada_serde import json_to_obj, obj_to_json
from dada_types import Parameters, SerializableObject, T


class FunctionContext(SerializableObject):
    """
    The function context is passed to every function.
    It provides access to the tables and the dada API client.
    """

    __module__ = "dada_plugin.core"

    def __init__(self, trace_id: Optional[str] = None, **kwargs):
        self.redis = redis
        self.trace_id = trace_id or str(uuid4())
        self.api = DadaClient(**kwargs)

    @property
    def log(self):
        return logging.getLogger(self.trace_id)


class Function(SerializableObject):

    __module__ = "dada_plugin.core"

    # input schema
    input_entity_types: T.entity_type_array.py = ["all"]
    input_file_types: T.file_type_array.py = ["all"]
    input_file_subtypes: T.file_subtype_array.py = ["all"]

    # output schema
    output_entity_types: T.entity_type_array.py = ["all"]
    output_file_types: T.file_type_array.py = ["all"]
    output_file_subtypes: T.file_subtype_array.py = ["all"]

    # parameters this function accepts
    param_def: T.param_def.py = {}

    # fields this function requires/installs.py
    required_fields: List[Dict[str, Any]] = []

    # folders this function requires/installs.py
    required_folders: List[Dict[str, Any]] = []

    # tags this function requires/installs.py
    required_tags: List[Dict[str, Any]] = []

    # desktops this function requires/installs.py
    required_desktops: List[Dict[str, Any]] = []

    # plugins this function requires/installs.py
    required_plugins: List[Dict[str, Any]] = []

    # functions this function requires/installs.py
    required_functions: List[Dict[str, Any]] = []

    # automations this function requires/installs.py
    required_automations: List[Dict[str, Any]] = []

    def __init__(self, *args, **kwargs):
        self.param_vals: Parameters = Parameters(self.param_def)

    def setup(self, context, **params) -> None:
        """
        The function's setup logic.
        """
        pass

    def exec(
        self, input: Any, context: FunctionContext, **params
    ) -> Tuple[Any, FunctionContext]:
        """
        The function's main logic. Each dada function must implement this method.
        """
        raise NotImplementedError(f"{self.object_name}.exec() not implemented")

    def shutdown(self, context: FunctionContext, **params) -> None:
        pass

    @property
    def required_objects(self):
        return {
            "fields": self.required_fields,
            "folders": self.required_folders,
            "tags": self.required_tags,
            "desktops": self.required_desktops,
            "plugins": self.required_plugins,
            "functions": self.required_functions,
            "automations": self.required_automations,
        }

    def _install_required_object(self, context: FunctionContext, obj_type, obj):
        """
        Install a required object via the API.
        """
        context.log.info(f"Creating {obj_type}: {obj}")
        return context.api.__getattribute__(obj_type).upsert(**obj)

    def install_required_objects(self, context: FunctionContext) -> None:
        """
        Install all required objects.
        """
        for obj_type, objects in self.required_objects.items():
            for obj in objects:
                self._install_required_object(context, obj_type, obj)


    def install(
        self, context: Optional[FunctionContext] = None, **params
    ) -> None:
        """
        The function's installation logic. This is run when the function is first installed.
        """
        if not context:
            context = FunctionContext(**params)
        self.install_required_objects(context)

    def run(
        self, input: Any, context: Optional[FunctionContext] = None, **params
    ) -> Tuple[Any, FunctionContext]:
        """
        Run the function with the given parameters."""
        params: T.param_val.py = self.param_vals.validate(**params)
        if not context:
            context = FunctionContext(**params)
        self.setup(context, **params)
        context.log.info(f"Running function {self.object_name} with params {params}")
        output, context = self.exec(input, context, **params)
        self.shutdown(context, **params)
        return output, context

    def cli(self):
        parser = argparse.ArgumentParser(prog=self.object_name)
        parser.add_argument('input', nargs='?', type=str, default=sys.stdin)
        parser.add_argument('output', nargs='?', type=argparse.FileType('w'), default=sys.stdout)
        self.param_vals.add_to_argparser(parser)
        args = parser.parse_args()
        params = args.__dict__
        input = params.pop("input")
        # read json from stdin
        if not isinstance(input, str):
            input = []
            for line in input.readline():
                input.append(json_to_obj(line))

            input = json_to_obj(input.read())
        output = params.pop("output")
        result, context = self.run(input, **params)
        output.write(obj_to_json(result))

# convenience classes
class FileFunction(Function):

    # input schema
    input_entity_types: T.entity_type_array.py = ["file"]

    # output schema
    output_entity_types: T.entity_type_array.py = ["file"]


class AudioFileFunction(FileFunction):

    # input schema
    input_file_types: T.file_type_array.py = ["audio"]

    # output schema
    output_file_types: T.file_type_array.py = ["audio"]


class BundleFileFunction(FileFunction):

    # input schema
    input_file_types: T.file_type_array.py = ["bundle"]

    # output schema
    output_file_types: T.file_type_array.py = ["bundle"]
