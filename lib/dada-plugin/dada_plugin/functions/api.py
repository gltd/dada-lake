"""
Built-in Convenience functions for reading/writing from the Dada API
"""
from dada_plugin.core import FunctionContext, Function, FileFunction


class DownloadFile(Function):
    """
    Ensure a file is downloaded to the local filesystem.
    """

    __module__ = "dada_plugin.functions.api"

    input_file_types = ["all"]
    input_file_subtypes = ["all"]

    output_file_types = ["all"]
    output_file_subtypes = ["all"]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def run(self, input, context, **params):
        pass