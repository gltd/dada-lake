import markdown


def md_to_html(md: str):
    """
    Convert a markdown string into html.
    :param md: A string containing markdown
    :return str
    """
    return markdown(md)
