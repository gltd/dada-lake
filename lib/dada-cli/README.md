# dada-cli

dada-lake's core cli, including access to the api, import/export, and conversion utilities

```
pip3 install dada-cli
dada --help
```

## TODO

- [ ] Pipelines https://click.palletsprojects.com/en/7.x/commands/#multi-command-pipelines