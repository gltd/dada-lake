from dada_plugin import AudioFileFunction

from dada_audio.id3 import id3_extract_fields

class Id3ExtractFields(AudioFileFunction):
    """
    Tag an audio file with ID3 tags.
    """

    __module__ = "dada_audio.functions.id3"

    param_def = {
        "defaults": {
            "name": "defaults",
            "abbr": "d",
            "type": "dict",
            "info": "Default values for ID3 tags.",
            "default": {},
            "required": False,
            "options": None,
        },

    }


    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def run(self, input, context, **params):
        pass