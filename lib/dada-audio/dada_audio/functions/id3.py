from dada_plugin import AudioFileFunction

from dada_audio.id3 import id3_extract_fields
import dada_settings

from dada_settings.utils import join_fields

# /////////////////
# ID3 AUDIO FIELDS
# ////////////
ID3_FIELDS_PREFIX = ""  # we don't want a prefix for ID3 fields because they're our source of truth.
                        # other field extraction methods can override these fields if they want to.

ID3_FIELD_PROPS = {
    "accepts_entity_types": ["file"],
    "accepts_file_types": ["audio"],
}

ID3_FIELDS_INIT = [
    # /////////////////
    # ID3 TAGS
    # ////////////
    # SEARCHABLE
    {"name": "artist_name", "type": "text", "is_searchable": True},
    {"name": "original_artist_name", "type": "text"},
    {"name": "album_artist_name", "type": "text"},
    {"name": "track_title", "type": "text", "is_searchable": True},
    {"name": "album_name", "type": "text", "is_searchable": True},
    {"name": "original_album_name", "type": "text"},
    {"name": "genre", "type": "text"},
    {"name": "label_name", "type": "text"},
    {"name": "comment", "type": "text"},
    {"name": "encoded_by", "type": "text"},
    # Integers
    {"name": "bit_rate", "type": "int"},
    {"name": "sample_rate", "type": "int"},
    {"name": "track_num", "type": "int"},
    {"name": "track_total", "type": "int"},
    {"name": "disc_num", "type": "int"},
    {"name": "disc_total", "type": "int"},
    {"name": "bytes", "type": "int"},
    {"name": "year", "type": "int"},
    {"name": "compilation", "type": "int"},
    {"name": "num_measures", "type": "int"},
    {"name": "num_bars", "type": "int"},
    # Dates
    {"name": "release_date", "type": "date_tz"},
    {"name": "original_date", "type": "date_tz"},
    # URLs (not type enforced)
    {"name": "webpage", "type": "text"},
    {"name": "publisher_webpage", "type": "text"},
    {"name": "url", "type": "text"},
    # Numeric
    {"name": "bpm", "type": "num"},
    {"name": "duration", "type": "num"},
    # Boolean
    {"name": "is_stereo", "type": "bool"},
    # ENUM
    {
        "name": "musical_key",
        "type": "text",
        # "options": AUDIO_DEFAULTS_VALID_KEYS, # TODO: Figure out this bug
    },
    {
        "name": "harmonic_code",
        "type": "text",
        # "options": AUDIO_DEFAULTS_VALID_HARMONIC_CODES, # TODO: Figure out this bug
    },
]

ID3_FIELDS = join_fields(
    ID3_FIELDS_PREFIX,
    ID3_FIELDS_INIT,
    ID3_FIELD_PROPS,
)



class Id3ExtractFields(AudioFileFunction):
    """
    Tag an audio file with ID3 tags.
    """

    __module__ = "dada_audio.functions.id3"

    param_def = {
        "bpm_decimals": {
            "abbr": "b",
            "type": "int",
            "info": "Number of decimals to round BPM to.",
            "default": dada_settings.AUDIO_DEFAULTS_BPM_DECIMALS,
            "required": False,
        }
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def exec(self, input, context, **params):
        context.log.info(f"Extracting id3 fields from {input}")
        fields = id3_extract_fields(input, **params)
        return fields, context


if __name__ == "__main__":
    f = Id3ExtractFields()
    f.cli()
