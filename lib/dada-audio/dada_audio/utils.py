"""
Audio/Music theory utilities
"""

from typing import Union, NewType

import dada_settings

# ///////////////////
# Custom types
# ///////////////////

RawId3Tag = NewType("RawId3Tag", str)


class Id3Error(ValueError):
    pass


# ///////////////////
# Reusable Doc Strings
# ///////////////////

RAW_TAG_PARAM = ":param raw_tag: A raw id3 tag to process"
BPM_PARAM = ":param bpm: The track's bpm (eg: ``120.0``)"
BPM_DECIMALS_PARAM = ":param bpm_decimals: The number of decimals to round the bpm to. (eg: `2`)"
DURATION_PARAM = ":param duration: The track's duration  (eg: ``240.0``)"
BPM_DURATION_PARAM = f"{BPM_PARAM}\n{DURATION_PARAM}"

# ///////////////////
# Functions
# ///////////////////


def key_to_harmonic_code(key: str) -> Union[str, None]:
    """
    Get a a harmonic code (eg: ``1A``) from a ``musical_key`` (eg: ``Am``)
    :param key: A musical key (eg: ``Am``)
    :return str
    """
    return dada_settings.AUDIO_DEFAULTS_KEYS_TO_HARMONIC_CODES.get(key, None)


def harmonic_code_to_key(harmonic_code: str) -> Union[str, None]:
    """
    Get a a key (eg: ``Am``) from a ``harmonic_code`` (eg: ``1A``)
    :param harmonic_code: A harmonic code (eg: ``1A``)
    :return str
    """
    return dada_settings.AUDIO_DEFAULTS_HARMONIC_CODES_TO_KEYS.get(
        harmonic_code, None
    )


def get_number_of_measures(bpm: float, duration: float) -> int:
    f"""
    Get the number of measures in a song given its bpm / duration
    Formula From: [Music Duration Calculator](https://www.vcalc.com/wiki/Coder/Music+Duration+Calculator)
    ```
    duration = (measures / bpm ) * 60
    measures = ((duration / 60) * bpm ) = measures
    ```
    {BPM_DURATION_PARAM}
    :return int
    """
    return int(round(((duration / 60) * bpm), 0))


def get_number_of_bars(
    bpm: float, duration: float, bar_length: int = 4
) -> float:
    f"""
    Get the estimated number of bars in a track given its bpm and duration and the time signature
    {BPM_DURATION_PARAM}
    :param bar_length: The number of measures in a bar.
    :return float
    """
    return get_number_of_measures(bpm, duration) / float(bar_length)
