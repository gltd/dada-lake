


# devving


deps_osx:

	brew bundle

install:
	@pip install -r requirements-dev.txt
	@make lib-dev
	@pip3 install -e lib/dada-settings
	@pip3 install -e .

dev:
	
	@echo "Setting up dev environment"
	make install 
	make db-recreate env=dev
	make db-upgrade env=dev
	make db-create-defaults env=dev

shell:

	@flask shell

lint:

	@find . -name "*.py" -not -path "./.venv/*" | xargs black

test:

	@DADA_ENV=test pytest -p no:warnings

test-one:

	@DADA_ENV=test pytest -p no:warnings $(t)

reqs-freeze:

	pip freeze | grep -wv dada > requirements.txt

reqs-upgrade:

	pip install -r requirements.txt -r requirements-dev.txt --upgrade


# library versioning / pypi 

lib-bump-versions:

	@scripts/lib-bump-versions.py


lib-dev:

	@pip install --global-option=build_ext --global-option="-I/usr/local/include/" --global-option="-L/usr/local/lib" pytaglib
	@pip install git+https://github.com/abelsonlive/marshmallow-sqlalchemy.git#egg=marshmallow-sqlalchemy
	@pip install -e lib/dada-settings && pip install -e lib/dada-text && pip install -e lib/dada-types && pip install -e lib/dada-errors && pip install -e lib/dada-plugin && pip install -e lib/dada-log && pip install -e lib/dada-utils
	@python ./scripts/lib-install-dev.py


lib-dist:

	@scripts/lib-dist.py


# swagger codegen

lib-gen-js-client:

	@rm -rf lib/swagger/js;
	swagger-codegen generate -i http://localhost:3030/spec.json -l javascript -o lib/swagger/js/

lib-gen-python-client:

	@rm -rf lib/swagger/python;
	swagger-codegen generate -i http://localhost:3030/spec.json -l python -o lib/swagger/python


# db 
db-init:

	@DADA_ENV='$(env)' flask db init

db-migrate:

	@DADA_ENV='$(env)' flask db migrate

db-upgrade:

	@DADA_ENV='$(env)' flask db upgrade

db-create-defaults:

	@DADA_ENV='$(env)' flask db-create-defaults

db-recreate:

	@dropdb dada_dev || true
	@createdb dada_dev
	@DADA_ENV='$(env)' flask db upgrade

db-recreate-test:

	@dropdb dada_test || true
	@createdb dada_test
	@DADA_ENV='test' flask db upgrade

db-create-schema-diagram:

	@scripts/create-db-schema.sh

db-view-schema-diagram:

	@open migrations/db-schema.pdf

# serving / background workers

run-flask:

	@DADA_ENV='$(env)' flask run -h 0.0.0.0 -p 3030

run-gunicorn:

	@DADA_ENV='$(env)' gunicorn dada.cli.admin:app -b 0.0.0.0:3030 -w 4 -k gevent -t 300

run-helper:

	@DADA_ENV='$(env)' celery -A scripts.celery_helper.celery  worker -l info

run-listener:

	@DADA_ENV='$(env)' python -m dada.queue.event_stream

# docker

docker-build:

	@docker-compose build

docker-up:

	@docker-compose up -d

docker-logs:

	@docker-compose logs -f